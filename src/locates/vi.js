import users from "./vi-Vi/users";

export default {
  // Định nghĩa thông tin chung cho phần thông báo
  "api.message.infoError": "Lấy thông tin xác thực thất bại!",
  "api.message.infoAfterCreateError": "Lỗi không lấy được bản ghi mới sau khi tạo thành công",
  "api.message.infoAfterEditError": "Lấy thông tin sau khi thay đổi thất bại",
  "api.message.notExisted": "Bản ghi này không tồn tại!",

  mobile: "Số điện thoại",
  placesId: "ID cơ sở",
  users: "Tài khoản ",
  usersId: "Id tài khoản",
  wardsId: 'Id Phường xã',
  status: "Trạng thái",
  createDate: 'Ngày tạo',
  FromDate: 'Ngày bắt đầu tìm kiếm',
  ToDate: 'Ngày kết thúc tìm kiếm',

  /* ĐỊNH NGHĨA THÔNG TIN RIÊNG CỦA TỪNG PHẦN */

  // users
  ...users
};
